# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/eclass/base.eclass,v 1.35 2008/11/09 15:47:47 loki_val Exp $

# @ECLASS: bsc.eclass
# @MAINTAINER:
# Peter Alfredsen <loki_val@gentoo.org>
#
# Based off base.eclass which was originally authored by Dan Armak
# @BLURB: The BaSiC eclass defines some default functions and variables.
# @DESCRIPTION:
# The bsc eclass makes life easier by requiring less code from the developer.
#
# NOTE: You must define EAPI=2 before inheriting from basic.


inherit eutils autotools libtool

# @ECLASS-VARIABLE: ORIG_EAPI
# @DESCRIPTION:
# Read-only variable used to verify that noone is tinkering with the EAPI
# variable.
ORIG_EAPI=${EAPI:-0}

case "${EAPI:-0}" in
	0|1)
		EXPORT_FUNCTIONS src_unpack src_compile src_install
		;;
	2)
		EXPORT_FUNCTIONS src_unpack src_prepare src_configure src_compile src_install
		;;
esac

DESCRIPTION="Based on the $ECLASS eclass"

# @FUNCTION: bsc_src_unpack
# @DESCRIPTION:
# This function unpacks (and runs bsc_src_prepare if EAPI is 0 or 1)
bsc_src_unpack() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	[[ "${A}" != "" ]] &&  unpack ${A}

	case "${EAPI:-0}" in
		0|1)
			debug-print "Running bsc_src_prepare"
			cd "${S}"
			bsc_src_prepare
			;;
		*)
			debug-print "Nothing more to do, since EAPI not 0 or 1"
			;;
	esac

}

# @FUNCTION: bsc_src_prepare
# @DESCRIPTION:
# Runs bsc_autopatch and bsc_autotools_run
bsc_src_prepare() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	bsc_autopatch
	bsc_autotools_run
}

# @FUNCTION: bsc_autotools_run
# @DESCRIPTION:
# Runs autotools, based on the contents of RUN_AUTOTOOLS and LIBTOOLIZE
bsc_autotools_run() {

	debug-print-function ${FUNCNAME} "$@"
	debug-print "RUN_AUTOTOOLS=${RUN_AUTOTOOLS[@]}"
	bsc_eapi_consistency_check

	local command
# @ECLASS-VARIABLE: RUN_AUTOTOOLS
# @DESCRIPTION:
# Which autotools should be run?
# Can be set to automake, autoconf, aclocal, autoheader, eautoreconf and
# libtoolize.
	if [[ -n "${RUN_AUTOTOOLS[@]}" || -n "${LIBTOOLIZE[@]}" ]]
	then
		for command in ${RUN_AUTOTOOLS[@]}
		do
			case "${command}" in
				automake|eautomake)
					debug-print "${FUNCNAME}: running eautomake"
					eautomake
					;;
				autoconf|eautoconf)
					debug-print "${FUNCNAME}: running eautoconf"
					eautoconf
					;;
				aclocal|eaclocal)
					debug-print "${FUNCNAME}: running eaclocal"
					eaclocal
					;;
				autoheader|eautoheader)
					debug-print "${FUNCNAME}: running eautoheader"
					eautoheader
					;;
				autoreconf|eautoreconf)
					debug-print "${FUNCNAME}: running eautoreconf"
					eautoreconf
					;;
				libtoolize|elibtoolize)
					debug-print "${FUNCNAME}: running elibtoolize"
					elibtoolize
					;;
				*)
					eerror "${FUNCNAME}: ${1} is invalid for RUN_AUTOTOOLS"
					die "${FUNCNAME}: ${1} is invalid for RUN_AUTOTOOLS"
					;;
			esac
		done

# @ECLASS-VARIABLE: LIBTOOLIZE
# @DESCRIPTION:
# If set to 'always', libtoolize will always be run. Handy for making sure .so
# versioning stays sane even on *bsd.
		if [[ "${LIBTOOLIZE[@]}" = "always" ]]
		then
			einfo "Running elibtoolize to preserve sanity of .so versioning."
			elibtoolize
		fi

	else
		debug-print "${FUNCNAME}: Nothing to do, returning.."
	fi
}

# @FUNCTION: bsc_autopatch
# @DESCRIPTION:
# Autopatches, based on the contents of the PATCHES array.
bsc_autopatch() {

	debug-print-function $FUNCNAME "$@"
	debug-print "$FUNCNAME PATCHES=$PATCHES"
	bsc_eapi_consistency_check

	local patch

# @ECLASS-VARIABLE: PATCHES
# @DESCRIPTION:
# An array of patches to be applied.
	if [[ "${PATCHES[@]}" ]]
	then
		if bsc_isarrayordie PATCHES
		then

			for patch in "${PATCHES[@]}"
			do
				debug-print "$FUNCNAME: autopatch: patching from ${x}"
				epatch "${patch}"
			done
		fi
	else
		debug-print '$PATCHES is empty, returning...'
		return 0
	fi
}

# @FUNCTION: bsc_src_configure
# @DESCRIPTION:
# The bsc src_prepare function, which is exported when [ EAPI >=2 ].
# Runs econf with parameters from bsc_use_enable_expand, bsc_use_with_expand
# and ECONF_SUFFIX appended.
bsc_src_configure() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	if [[ -x "${ECONF_SOURCE:-.}"/configure ]]
	then

# @ECLASS-VARIABLE: ECONF_SUFFIX
# @DESCRIPTION:
# Extra bits you want tagged onto your econf commmand.
		econf $(bsc_use_enable_expand) $(bsc_use_with_expand) "${ECONF_SUFFIX[@]}"
	else
		[[ -n "${USE_ENABLE[@]}" || -n "${USE_WITH[@]}" ]] &&  die "USE_ENABLE or USE_WITH non-empty, but no configure found"
		debug-print "no ${ECONF_SOURCE:-.}/configure found, returning..."
	fi

}

# @FUNCTION: bsc_use_enable_expand
# @DESCRIPTION:
# Expands USE_ENABLE array to --disable/--enable switches.
bsc_use_enable_expand() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	local _use
	if bsc_isarrayordie USE_ENABLE
	then
# @ECLASS-VARIABLE: USE_ENABLE
# @DESCRIPTION:
# An array of use_enable arguments to be expanded and tagged onto your econf
# command.
		for _use in "${USE_ENABLE[@]}"
		do
			printf " $(use_enable ${_use})"
		done
	fi
}

# @FUNCTION: bsc_use_with_expand
# @DESCRIPTION:
# Expands USE_WITH array to --with/--without switches.
bsc_use_with_expand() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	local _use
	if bsc_isarrayordie USE_WITH
	then
# @ECLASS-VARIABLE: USE_WITH
# @DESCRIPTION:
# An array of use_with arguments to be expanded and tagged onto your econf
# command.
		for _use in "${USE_WITH[@]}"
		do
			printf " $(use_with ${_use})"
		done
	fi
}


# @FUNCTION: bsc_src_compile
# @DESCRIPTION:
# The bsc src_compile function, which is exported.
# Runs emake and if [ EAPI < 2 ] also bsc_src_configure.
bsc_src_compile() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	debug-print-section "$FUNCNAME: EAPI-dependent action. EAPI=${EAPI}"
	case "${EAPI:-0}"  in
		0|1)
			debug-print "Running bsc_src_configure"
			bsc_src_configure
			debug-print "Done"
			;;
		*)
			debug-print "Continuing, nothing to do here."
			;;
	esac

	debug-print-section "$FUNCNAME: compile"
	if [[ -f Makefile || -f GNUmakefile || -f makefile ]]
	then
# @ECLASS-VARIABLE: COMPILE_EMAKE_PARAMS
# @DESCRIPTION:
# A variable containing arguments to be passed to emake in src_compile
		emake ${COMPILE_EMAKE_PARAMS[@]} || die "died running emake $FUNCNAME: emake"
	fi
}

# @FUNCTION: bsc_src_install
# @DESCRIPTION:# The bsc src_install function, which is exported.
# Respects DOCS variable.

bsc_src_install() {

	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	debug-print-section "$FUNCNAME: emake install"
	if [[ -f Makefile || -f GNUmakefile || -f makefile ]]
	then
# @ECLASS-VARIABLE: INSTALL_EMAKE_PARAMS
# @DESCRIPTION:
# A variable containing arguments to be passed to emake in src_install.
# Defaults to -j1. For good reason.
		emake ${INSTALL_EMAKE_PARAMS[@]:--j1} DESTDIR="${D}" install || die "died running emake install, $FUNCNAME:emake"
	fi

}

# @FUNCTION: bsc_eapi_consistency_check
# @DESCRIPTION:
# Makes sure nobody messes with the EAPI variable.
bsc_eapi_consistency_check() {

	debug-print-function $FUNCNAME "$@"

	if [[ "${ORIG_EAPI}" = "${EAPI:-0}" ]]
	then
		debug-print "EAPI Verified."
	else
		die "EAPI has been redefined after bsc eclass was inherited. This is not allowed!"
	fi
}

# @FUNCTION: bsc_isarray
# @DESCRIPTION:
# Checks $@ to see if ALL parameters are arrays
bsc_isarray() {
	debug-print-function $FUNCNAME "$@"
	bsc_eapi_consistency_check

	if [[ "$@" ]]
	then
		for posarray in $@
		do
			if ! [[ "$(declare -p $posarray)" = 'declare -a '* ]] && declare -p $posarray &> /dev/null
			then
				return 1
			fi
		done
	else
		die "you're calling $FUNCNAME without any parameters. Disallowed."
	fi
}
# @FUNCTION: bsc_isarrayordie
# @DESCRIPTION:
# Calls bsc_isarray. Dies if unsuccesful.
bsc_isarrayordie() {
	debug-print-function $FUNCNAME "$@"
	if ! bsc_isarray "$@"
	then
		eerror "$posarray needs to be a BASH array, like so:"
		eerror "$posarray"'=( "BLAH" "BLURG" "BALG" )'
		die "$posarray needs to a BASH array!"
	fi
}
