# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-sound/dvda-author/dvda-author-20050703.ebuild,v 1.1 2008/01/06 02:46:01 sbriesen Exp $

EAPI=2

inherit eutils toolchain-funcs bsc

YEAR=${PV:2:2}
MONTH=${PV:4:2}
DAY=${PV:6:2}

DESCRIPTION='Author a DVD-Audio DVD'
HOMEPAGE='http://dvd-audio.sourceforge.net/'
SRC_URI="mirror://sourceforge/dvd-audio/${PN}-${YEAR}.${MONTH}-${DAY}-dev.tar.bz2"

LICENSE='GPL-2'
SLOT='0'
KEYWORDS='~amd64 ~x86'
IUSE=''

S=${WORKDIR}/${PN}-${YEAR}.${MONTH}

RDEPEND='>=media-libs/flac-1.1.3
	media-libs/libogg'

RUN_AUTOTOOLS=( "autoreconf" )

PATCHES=(	"${FILESDIR}/${P}-configure.patch"
		"${FILESDIR}/${P}-strip.patch"	)

#ECONF_SUFFIX=( "--with-ogg=-logg" "--with-flac='-lFLAC -lm'" )
