# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit toolchain-funcs eutils

DESCRIPTION="A lightweight gtk2 editor with features for programming such as website editing and coding.  Also includes a mini macro language."
HOMEPAGE="http://www.nongnu.org/beaver/index.html"
MY_P=${PN}${PV//./_}
SRC_URI="http://download.savannah.gnu.org/releases/beaver/0.3.0.1%20-%202008.06.26/${MY_P}.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
RDEPEND=">=x11-libs/gtk+-2"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"
S=${WORKDIR}/${MY_P}

src_compile() {
	emake	CC="$(tc-getCC)" \
		CFLAGS="${CFLAGS} -Wall $(pkg-config gtk+-2.0 --cflags)" \
		LDFLAGS="${LDFLAGS} $(pkg-config gtk+-2.0 --libs)" \
		DESTDIR="${D}/usr" \
		DBUG="-Wall" || die "emake failed"


}

src_install() {
	dobin src/beaver
	doman beaver.1x
	insinto /usr/share/beaver/pixmaps
	doins pixmaps/*
	insinto /share/beaver/bl
	doins bl/*
	dodoc ChangeLog NEWS README TODO THANKS BUGS AUTHORS
	make_desktop_entry "beaver" "Beaver" "/usr/share/beaver/pixmaps/beaver.png" "Application;Development;TextEditor;GTK"

}
