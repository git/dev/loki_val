# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=1

inherit eutils toolchain-funcs

DESCRIPTION="A Solaris-sh based Bourne Shell, installed as 'jsh'."
HOMEPAGE="http://foo.bar.com/"

SRC_URI="mirror://sourceforge/heirloom/${P}.tar.bz2"

LICENSE="CDDL"

SLOT="0"

KEYWORDS="~amd64"

IUSE="+spell"

DEPEND=""
RDEPEND="${DEPEND}"

src_compile() {
	emake -f	makefile \
			CC="$(tc-getCC)" \
			CFLAGS="${CFLAGS} -Wall" \
			CPPFLAGS="-D_GNU_SOURCE" \
			LARGEF="-D_FILE_OFFSET_BITS=64" \
			SPELL="$(use spell&&echo '-DSPELL')" \
			sh sh.1.out || die "emake failed"
}

src_install() {
	exeinto /bin
	newexe sh jsh
	newman sh.1.out jsh.1
	dodoc CHANGES README || die "dodoc failed"
}

pkg_postinst() {
	elog "${P} has been installed as /bin/jsh. If you want to use ${PN} as your /bin/sh, do:"
	elog "ln -sf jsh /bin/sh"
}
